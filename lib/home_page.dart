import 'package:flutter/material.dart';
import 'package:product_animation_app/my_animated_widget.dart';
import 'product_box.dart';

class MyHomePage extends StatelessWidget {

  MyHomePage({Key? key, required this.title, required this.animation}): super(key: key);

  final String title;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product Listing'),
      ),
      body: ListView(
        shrinkWrap: true,
          padding: const EdgeInsets.fromLTRB(2.0, 10.0, 2.0, 10.0),
        children: [
          FadeTransition(
            child: ProductBox(
              name: "iphone",
              description: "iphone is the stylist phone ever",
              price: 1000,
              image: "iphone.jpg",
            ),
            opacity: animation),
          MyAnimatedWidget(child: ProductBox(
            name: "Pixel",
            description: "Pixel is the most featreful phone ever",
            price: 800,
            image: "Pixel.jpg",
          ), animation: animation),
          ProductBox(name: "Laptop", description: "Laptop is the most productive development tool", price: 2000, image: "laptop.jpg"),
          ProductBox(name: "Tablet", description: "Tablet is the most useful device ever for meeting", price: 1500, image: "tablet.jpg"),
          ProductBox(name: "Pendrive", description: "Pendrive is useful storage medium", price: 100, image: "pendrive.jpg"),
          ProductBox(name: "Floppy Drive", description: "Floppy drive is useful rescue storage medium", price: 20, image: "floppy.jpg")
        ],
      ),
    );
  }
}

